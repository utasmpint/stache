# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

We try to adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), although
some early releases do not.

## [v1.2.0] - 2022-11-03
### Improvements
 - Changes to allow get_credential to support credentials that do not store passwords as dictionaries.
   Now you can parse as a simple secret or even pass in your own parser.
 - Also added this changelog.

## [v1.1.0] - 2021-12-15
### Improvements
 - Support for the Stache API v2 -- added the GetCredential.from_endpoint method.

## [v1.0.3] - 2018-11-13
### Improvements
 - Added try limits and specific logic to deal with getting HTTP 429 codes from the
   stache API.

## [v1.0.2] - 2017-10-06
### Improvements
 - Added parse\_field\_as\_dict to api_request
 - Changed to only split on the first colon, so that embedded colons in passwords, etc. are legal.
 - Added more description on why the failing tests are failing.

## [v1.0.1] - 2016-07-05
### Improvements
- Made the stache library Python 2.7-compatible.


## [v1.0.0] - 2016-06-29
- Initial release
