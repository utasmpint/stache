from stache import api_request


class GetStacheEntry(object):
    def __init__(self, api_key, item_id, *, endpoint_url=None):

        if item_id is None and endpoint_url is None:
            raise ValueError('You must supply item_id (api v1) or '
                             'endpoint_url.')

        self.api_key = api_key
        self.item_id = item_id

        if endpoint_url is None:
            self.endpoint_url = api_request.create_v1_url(item_id)
        else:
            self.endpoint_url = endpoint_url

    @classmethod
    def from_endpoint(cls, *, endpoint_url, x_stache_key):
        """Virtual constructor to make endpoints more convenient."""
        gse = GetStacheEntry(x_stache_key, None, endpoint_url=endpoint_url)
        return gse

    def return_fields(self):
        '''Return purpose,secret, nickname, memo from stache entry.'''

        return api_request.retrieve_from_endpoint(
            endpoint_url=self.endpoint_url,
            x_stache_key=self.api_key
        )
