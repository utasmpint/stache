class StacheError(Exception):
    "Base class for Stache Errors."

class StacheValueError(StacheError):
    "Parameter error in a call to the Stache library."


class StacheFormatError(StacheError):
    pass


class TryLimitError(StacheError):
    pass
