from collections import OrderedDict
import json
import warnings
import unittest
from unittest.mock import Mock, patch

from testfixtures import LogCapture

from stache import api_request
from stache.errors import StacheFormatError, TryLimitError
from stache.tests.get_credentials_for_tests import get_config

config = get_config()

class TestApiRequest(unittest.TestCase):
    def test_logging_exists(self):
        with LogCapture() as lc:
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                _ = api_request.retrieve_creds(
                    config.stache_v1_api_key,
                    str(config.stache_v1_item_id)
                )


            log_tuples = list(lc.actual())
            self.assertTrue(len(log_tuples) >= 1)
            log_msg = log_tuples[0][2]
            self.assertTrue('attempting to call stache API' in log_msg)

    def test_logging_of_requests_exceptions(self):
        class ExpectedError(Exception):
            pass
        with LogCapture() as lc:
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                with patch('stache.api_request.requests.get') as mock_get:
                    def getf(*args, **kwargs):
                        raise ExpectedError('Go boom!')
                    mock_get.side_effect = getf

                    try:
                        _ = api_request.retrieve_from_endpoint(
                            endpoint_url=config.stache_v2_endpoint,
                            x_stache_key=config.stache_v2_x_stache_key
                        )
                    except ExpectedError as ex:
                        self.assertEqual('Go boom!', str(ex))
                        log_tuples = list(lc.actual())
                        logtuple = log_tuples[1]
                        self.assertEqual('stache.api_request', logtuple[0])
                        self.assertEqual('ERROR', logtuple[1])
                        self.assertTrue('stache API call failed with exception'
                                        in logtuple[2]
                        )


            log_tuples = list(lc.actual())
            self.assertTrue(len(log_tuples) >= 1)
            log_msg = log_tuples[0][2]
            self.assertTrue('attempting to call stache API' in log_msg)

    def test_retrieve_creds(self):
        expected_list = [config.stache_v1_purpose,
                         config.stache_v1_secret,
                         config.stache_v1_nickname,
                         config.stache_v1_memo]

        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            stache_tuple = api_request.retrieve_creds(
                 config.stache_v1_api_key,
                 str(config.stache_v1_item_id)
            )
        result_list = list(stache_tuple)

        print('\n')
        print("This test will fail until a bug is fixed by stache "
              "(Purpose not returned)."
        )

        self.assertEqual(result_list, expected_list)

    def test_retrieve_from_endpoint(self):
        expected_list = [config.stache_v2_purpose,
                         config.stache_v2_secret,
                         config.stache_v2_nickname,
                         config.stache_v2_memo]

        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            stache_tuple = api_request.retrieve_from_endpoint(
                endpoint_url=config.stache_v2_endpoint,
                x_stache_key=config.stache_v2_x_stache_key
            )
        result_list = list(stache_tuple)

        print('\n')
        print("This test will fail until a bug is fixed by stache "
              "(Purpose not returned)."
        )

        self.assertEqual(result_list, expected_list)

    def test_try_limit_less_than_1_raises(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            try:
                _ = api_request.retrieve_from_endpoint(
                    endpoint_url=config.stache_v2_endpoint,
                    x_stache_key=config.stache_v2_x_stache_key,
                    try_limit=0
                )
                self.fail('Expected an error.')
            except ValueError as ve:
                self.assertTrue('try_limit' in str(ve))

    def test_try_limit_greater_than_try_max_raises(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            try:
                _ = api_request.retrieve_from_endpoint(
                    endpoint_url=config.stache_v2_endpoint,
                    x_stache_key=config.stache_v2_x_stache_key,
                    try_limit=api_request.TRY_MAX + 1
                )
                self.fail('Expected an error.')
            except ValueError as ve:
                self.assertTrue('try_limit' in str(ve))

    def test_try_sleep_greater_than_try_sleep_max_raises(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            try:
                _ = api_request.retrieve_from_endpoint(
                    endpoint_url=config.stache_v2_endpoint,
                    x_stache_key=config.stache_v2_x_stache_key,
                    try_sleep=api_request.TRY_SLEEP_MAX + 1
                )
                self.fail('Expected an error.')
            except ValueError as ve:
                self.assertTrue('try_sleep' in str(ve))

    def test_429_status_causes_multiple_tries(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            with LogCapture() as lc:
                with patch('stache.api_request.requests.get') as mock_get:
                    mock_response = Mock()
                    mock_response.ok = False
                    mock_response.status_code = 429
                    mock_get.return_value = mock_response

                    try:
                        _ = api_request.retrieve_from_endpoint(
                            endpoint_url=config.stache_v2_endpoint,
                            x_stache_key=config.stache_v2_x_stache_key,
                            try_sleep=0.1 # short for test w/mock
                        )
                        self.fail('try_limit somehow not exceeded -- error.')
                    except TryLimitError as tle:
                        self.assertTrue('exceeded' in str(tle))

                        log_tuples = list(lc.actual())

                        expected_msg = 'stache call failed with status code 429'
                        try_errors = 0
                        for lt in log_tuples:
                            if lt[2] == expected_msg:
                                try_errors += 1

                        self.assertEqual(api_request.DEFAULT_TRY_LIMIT,
                                         try_errors
                        )

    def test_500_status_does_not_cause_multiple_tries(self):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore') # we know this will gen. a warning
            with LogCapture() as lc:
                real_rfs = api_request.requests.Response.raise_for_status
                with patch('stache.api_request.requests.get') as mock_get:
                    mock_response = Mock()
                    mock_response.ok = False
                    mock_response.status_code = 500
                    mock_response.raise_for_status = real_rfs
                    mock_get.return_value = mock_response

                    try:
                        _ = api_request.retrieve_from_endpoint(
                            endpoint_url=config.stache_v2_endpoint,
                            x_stache_key=config.stache_v2_x_stache_key,
                            try_sleep=0.1 # short for test w/mock
                        )
                        self.fail('try_limit somehow not exceeded -- error.')
                    except Exception as exc:
                        log_tuples = list(lc.actual())

                        expected_msg = 'stache call failed with status code 429'
                        try_errors = 0
                        for lt in log_tuples:
                            if lt[2] == expected_msg:
                                try_errors += 1

                        self.assertEqual(0, try_errors)

    def test_parse_response(self):
        data = ({'purpose': 'test1',
                 'secret': 'blah',
                 'nickname': 'bert',
                 'memo': 'memo memo'})
        expected_list = ['test1',
                         'blah',
                         'bert',
                         'memo memo']
        response = json.dumps(data)

        (purpose,
         secret,
         nickname,
         memo) = api_request.parse_response(response.encode('utf-8'))

        result_list = [purpose, secret, nickname, memo]

        self.assertEqual(result_list, expected_list)

    def test_parse_response_with_embedded_colons(self):
        data = ({'purpose': 'test1',
                 'secret': 'blah:blah',
                 'nickname': 'bert',
                 'memo': 'memo memo'})
        expected_list = ['test1',
                         'blah:blah',
                         'bert',
                         'memo memo']
        response = json.dumps(data)

        (purpose,
         secret,
         nickname,
         memo) = api_request.parse_response(response.encode('utf-8'))

        result_list = [purpose, secret, nickname, memo]

        self.assertEqual(result_list, expected_list)

    def test_unexpected_stache_structure_raises_format_errror(self):
        mock_content = 'something unexpected'.encode('utf-8')
        try:
            api_request.parse_response(mock_content)
            self.fail('Unexpected success in parsing garbage stache response')
        except StacheFormatError:
            pass


class TestParseFieldAsDict(unittest.TestCase):
    def test_parses_simple_dict(self):
        field = 'key1:value1\nkey2:value 2 \nkey3 : value3   \n\n'
        fd = api_request.parse_field_as_dict(field, value_delimiter=None)
        expected = OrderedDict([('key1', 'value1'),
                                ('key2', 'value 2'),
                                ('key3', 'value3')
        ])
        self.assertEqual(expected, fd)

    def test_parse_ignores_comments(self):
        field = ('#this is a comment about the dict below\n'
                 'key1:value1\n'
                 'key2: value2\n'
                 'key3: value3 #another comment\n'
                 ' #and another comment with a leading space\n'
        )
        fd = api_request.parse_field_as_dict(field)
        expected = OrderedDict([('key1', 'value1'),
                                ('key2', 'value2'),
                                ('key3', 'value3')
        ])
        self.assertEqual(expected, fd)

    def test_parse_splits_values(self):
        field = ('key1:value1a, value1b, value1c \n'
                 'key2: value2a, value2b , value2c\n'
                 'key3: value3 '
        )
        fd = api_request.parse_field_as_dict(field, value_delimiter=',')
        expected = OrderedDict([('key1', ['value1a', 'value1b', 'value1c']),
                                ('key2', ['value2a', 'value2b', 'value2c']),
                                ('key3', ['value3',])
        ])
        self.assertEqual(expected, fd)

    def test_key_delimiter_raises_if_none(self):
        field = ('key value')
        with self.assertRaises(ValueError):
            api_request.parse_field_as_dict(field, key_delimiter=None)

    def test_keys_returned_with_default_value(self):
        field = ('line1\nline2\nline3\n')
        fd = api_request.parse_field_as_dict(field)
        self.assertEqual(set(['line1', 'line2', 'line3']), fd.keys())
        self.assertEqual([None, None, None], list(fd.values()))

        fd = api_request.parse_field_as_dict(field, default_value='kumquat')
        self.assertEqual(set(['line1', 'line2', 'line3']), fd.keys())
        self.assertEqual(['kumquat', 'kumquat', 'kumquat'], list(fd.values()))

    def test_blank_lines_ignored(self):
        field = ('\n\n')
        fd = api_request.parse_field_as_dict(field)
        self.assertEqual(OrderedDict(), fd)

    def test_repeated_keys(self):
        field = ('key1: value1\nkey1: value2\nkey2: something\nkey1:value3\n')
        fd = api_request.parse_field_as_dict(field)
        self.assertEqual(set(['key1', 'key2']), fd.keys())
        self.assertEqual(['value1', 'value2', 'value3'], fd['key1'])
        self.assertEqual('something', fd['key2'])

    def test_key_repeats_false(self):
        field = ('key1: value1\nkey1: value2\nkey2: something\nkey1:value3\n')
        fd = api_request.parse_field_as_dict(field, key_repeats=False)
        self.assertEqual(set(['key1', 'key2']), fd.keys())
        self.assertEqual('value3', fd['key1'])
        self.assertEqual('something', fd['key2'])
