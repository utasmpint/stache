import unittest
from unittest.mock import Mock, patch

from stache.all_fields.get_stache_entry import GetStacheEntry
from stache.tests.get_credentials_for_tests import get_config

config = get_config()


class TestGetStacheEntry(unittest.TestCase):
    def test_both_item_id_and_endpoint_cannot_be_none(self):
        try:
            _ = GetStacheEntry(config.stache_v1_api_key, None)
            self.fail('Unexpected: did not raise ValueError')
        except ValueError:
            pass

        try:
            _ = GetStacheEntry.from_endpoint(
                x_stache_key=config.stache_v1_api_key,
                endpoint_url=None
            )
            self.fail('Unexpected: did not raise ValueError')
        except ValueError:
            pass

    @patch('stache.api_request.requests.get')
    def test_return_fields(self, mock_get):
        expected_list = [config.stache_v1_purpose,
                         config.stache_v1_secret,
                         config.stache_v1_nickname,
                         config.stache_v1_memo]

        mock_response = Mock()
        mock_response.ok = True
        mock_response.content = (
            '{"nickname":"stachelib_test_entry",'
            '"purpose":"Stable test entry for our stache library",'
            '"secret":"secret","memo":"memo"}'
        ).encode('utf-8') # make it bytes
        mock_get.return_value = mock_response

        reference = GetStacheEntry(config.stache_v1_api_key,
                                   str(config.stache_v1_item_id)
                                   )
        purpose, secret, nickname, memo = reference.return_fields()

        result_list = [purpose, secret, nickname, memo]

        print('\n')
        print("This test will fail until bug is fixed in stache API "
              "(Purpose not returned)."
        )

        self.assertEqual(result_list, expected_list)

    @patch('stache.api_request.requests.get')
    def test_get_stache_entry_can_take_endpoint(self, mock_get):
        expected_list = [config.stache_v1_purpose,
                         config.stache_v1_secret,
                         config.stache_v1_nickname,
                         config.stache_v1_memo]

        mock_response = Mock()
        mock_response.ok = True
        mock_response.content = (
            '{"nickname":"stachelib_test_entry",'
            '"purpose":"Stable test entry for our stache library",'
            '"secret":"secret","memo":"memo"}'
        ).encode('utf-8') # make it bytes
        mock_get.return_value = mock_response

        reference = GetStacheEntry.from_endpoint(
            endpoint_url=config.stache_v1_endpoint,
            x_stache_key=config.stache_v1_api_key
        )
        purpose, secret, nickname, memo = reference.return_fields()

        result_list = [purpose, secret, nickname, memo]

        print('\n')
        print("This test will fail until bug is fixed in stache API "
              "(Purpose not returned)."
        )

        self.assertEqual(result_list, expected_list)
