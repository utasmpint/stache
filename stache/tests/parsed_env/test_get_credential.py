import unittest

from stache.errors import StacheValueError
from stache.parsed_env.get_credential import (
    GetCredential,
    parse_secret_as_dict,
    simple_secret,
)
from stache.tests.get_credentials_for_tests import get_config

config = get_config()


class TestGetOracleCredentials(unittest.TestCase):
    def test_both_item_id_and_endpoint_cannot_be_none(self):
        try:
            _ = GetCredential(config.stache_v1_api_key, None, "DEV")
            self.fail('Unexpected: did not raise ValueError')
        except ValueError:
            pass

        try:
            _ = GetCredential.from_endpoint(
                x_stache_key=config.credential_x_stache_key,
                endpoint_url=None,
                env="DEV"
            )
            self.fail('Unexpected: did not raise ValueError')
        except ValueError:
            pass

    def test_ret_creds(self):

        reference = GetCredential.from_endpoint(
            endpoint_url=config.credential_endpoint,
            x_stache_key=config.credential_api_key,
            env="DEV"
        )
        nickname, password = reference.ret_creds()

        self.assertEqual([nickname, password], [config.credential_nickname,
                                                config.credential_dev_secret]
                         )

    def test_parse_secret(self):

        secret = "DEV: dev-password\nQUAL: qual-password\nPROD: prod-password"
        env = "QUAL"

        reference = GetCredential(
            config.credential_api_key,
            config.credential_item_id,
            env
        )
        password = reference.parse_secret(secret)

        self.assertEqual(password, "qual-password")

    def test_parse_secret_with_embedded_colons(self):

        secret = ("DEV: dev-password\n"
                  "QUAL: qual-password:with:colon\n"
                  "PROD: prod-password"
        )
        env = "QUAL"

        reference = GetCredential(
            config.credential_api_key,
            config.credential_item_id,
            env
        )
        password = reference.parse_secret(secret)

        self.assertEqual(password, "qual-password:with:colon")


class TestParseSecret(unittest.TestCase):
    def test_defaults_to_parsing_dict_with_env(self):
        gc = GetCredential('abc', '123', env='my_env')

        secret=("my_env: my_secret\n"
                "another_env:another_secret"
        )
        ret_secret = gc.parse_secret(secret)
        self.assertEqual("my_secret", ret_secret)

    def test_parse_simple_secret(self):
        gc = GetCredential('abc', '123', env='my_env')

        secret="DEADBEEF"
        ret_secret = gc.parse_secret(secret, secret_parser=simple_secret)
        self.assertEqual(secret, ret_secret)

    def test_parse_secret_with_args(self):
        gc = GetCredential('abc', '123', env='my_env')

        secret=("key1: secret1\n"
                "key2: secret2"
        )
        ret_secret = gc.parse_secret(secret, secret_key='key2')
        self.assertEqual("secret2", ret_secret)

    def test_parse_secret_as_dict_raises_on_missing_key(self):
        with self.assertRaises(StacheValueError):
            parse_secret_as_dict('akey: avalue', None)
