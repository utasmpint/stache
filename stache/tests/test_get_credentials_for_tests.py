from contextlib import redirect_stdout, redirect_stderr
from io import StringIO
import os
import sys
from unittest.mock import Mock, patch
import uuid

from pyfakefs.fake_filesystem_unittest import TestCase

from stache.tests.get_credentials_for_tests import find_config

MAGIC_KEY_VALUE = uuid.uuid4().hex

CONFIG_CONTENTS = f"""
magic_testing_key: '{MAGIC_KEY_VALUE}'
"""

class TestGetCredentialsForTests(TestCase):
    def setUp(self):
        self.setUpPyfakefs() # creates self.fs
        self.config_dir = '/config'
        self.fs.create_dir(self.config_dir)
        self.config_fp = os.path.join(self.config_dir, 'test_config.yaml')
        self.fs.create_file(self.config_fp, contents=CONFIG_CONTENTS)

        os.environ['STACHE_TEST_CONFIG'] = self.config_fp

    def test_find_config_with_environment_variable(self):
        config = find_config()
        self.assertEqual(MAGIC_KEY_VALUE,
                         getattr(config, 'magic_testing_key', None)
        )

    def test_find_config_without_environment_variable(self):
        with patch('os.getcwd') as mock_getcwd:
            mock_getcwd.return_value = self.config_dir

            with patch.dict(os.environ, values=(), clear=True):
                config = find_config()
                self.assertEqual(MAGIC_KEY_VALUE,
                                 getattr(config, 'magic_testing_key', None)
                )

    def test_find_config_works_with_user_input(self):
        with patch('os.getcwd') as mock_getcwd:
            empty_dir = '/empty'
            self.fs.create_dir(empty_dir)
            mock_getcwd.return_value = empty_dir
            with patch.dict(os.environ, values=(), clear=True):
                # no default path will work, so find_config will ask for
                # user input
                with patch(
                        'stache.tests.get_credentials_for_tests.input'
                ) as mock_input:
                    mock_input.return_value = self.config_fp
                    config = find_config()
                    self.assertEqual(
                        MAGIC_KEY_VALUE,
                        getattr(config, 'magic_testing_key', None)
                    )

    def test_find_config_prints_message_on_bad_user_path(self):
        with patch('os.getcwd') as mock_getcwd:
            empty_dir = '/empty'
            self.fs.create_dir(empty_dir)
            mock_getcwd.return_value = empty_dir
            with patch.dict(os.environ, values=(), clear=True):
                # no default path will work, so find_config will ask for
                # user input
                def value_gen():
                    # need a function that can return '' to end user input
                    for val in ['/no_dir_here/test_config.yaml']:
                        yield val
                    else:
                        yield ''

                vg = value_gen()
                def side_effect(*args, **kwargs):
                    nonlocal vg
                    val = next(vg)
                    print('............ returning', repr(val), '............')
                    return val

                with patch(
                        'stache.tests.get_credentials_for_tests.input'
                ) as mock_input:
                    mock_input.side_effect = side_effect
                    buf = StringIO()
                    with redirect_stderr(buf):
                        with redirect_stdout(buf):
                            config = find_config()
                    output = buf.getvalue()
                    buf.close()
                    print(output)
                    self.assertTrue('not a valid path' in output)
                    self.assertEqual(None, config)

    def test_find_config_user_input_lets_user_quit_on_blank(self):
        with patch('os.getcwd') as mock_getcwd:
            empty_dir = '/empty'
            self.fs.create_dir(empty_dir)
            mock_getcwd.return_value = empty_dir
            with patch.dict(os.environ, values=(), clear=True):
                # no default path will work, so find_config will ask for
                # user input
                with patch(
                        'stache.tests.get_credentials_for_tests.input'
                ) as mock_input:
                    mock_input.return_value = ''
                    config = find_config()
                    self.assertEqual(None, config)

    def test_bad_environment_variable_results_in_message(self):
        os.environ['STACHE_TEST_CONFIG'] = '/no_dir_here/test_config.yaml'
        buf = StringIO()
        with redirect_stdout(buf):
            config = find_config()

        output = buf.getvalue()
        buf.close()
        self.assertTrue('Unable to use the configuration path' in output)
        self.assertEqual(None, config)

    def test_yaml_not_available_returns_none_for_config(self):
        # emulate yaml not available in filesystem
        with patch.dict(sys.modules, {'yaml': None}):
            config = find_config()
            self.assertEqual(None, config)
